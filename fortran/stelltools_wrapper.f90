
! ==============================================================================
!
! AUTHOR:
!   Novimir Antoniuk Pablant
!
! DATE:
!   2011-09-15
!
! PURPOSE:
!   Create an interface to various useful VMEC tools from the LIBSTELL
!   library, inparticular the stel_tools module.
!
! DESCRIPTION:  
!   This is currently designed to use STELLOPT_v250.
!
!   Mirtools uses the following coordinate systems:
!     flux coordinates: s, u, phi
!     cylindrical coordinates: R, phi, Z
!   Where phi spans from 0 to 2pi over the full machine.
!
!   It is important to note that these flux coordinates are not the same
!   as those used internally by VMEC. VMEC instead uses (s, u, v) where v
!   spans from 0 to 2pi over one stellarator period.
!
! PROGRAMMING NOTES:
!   This wrapper is written to allow mirtools to be called from python using
!   f2py.  Currently f2py does not properly handle optional parameters or
!   interfaces. For this reason we need to recreate these features using
!   a workaround in this wrapper and an interface in python.
!
!   There is no simple fix since the implementaton of these FORTRAN features
!   is compiler dependent and at least in gfortran makes use of hidden
!   parameters. This may be fixed eventually in generation 3 of f2py (g3).
!
! ==============================================================================

!!$SUBROUTINE read_wout_file(filename, error_status)
!!$  USE read_wout_mod, ONLY: read_wout_file__wrap => read_wout_file
!!$  IMPLICIT NONE
!!$  CHARACTER(len=*), INTENT(IN) :: filename
!!$  INTEGER, INTENT(OUT) :: error_status
!!$
!!$  WRITE(*,*) 'Reading wout file:'
!!$  WRITE(*,*) '  ', TRIM(filename)
!!$
!!$  CALL read_wout_file__wrap(TRIM(filename), error_status)
!!$
!!$END SUBROUTINE read_wout_file


! ------------------------------------------------------------------------------
! Begin INTERFACE initialize_from_vmec.
! ------------------------------------------------------------------------------

SUBROUTINE initialize_from_wout_000(error_status)
  USE stelltools, ONLY: initialize_from_wout__wrap => initialize_from_wout
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  error_status = 0
  CALL initialize_from_wout__wrap(error_status)
END SUBROUTINE initialize_from_wout_000

SUBROUTINE initialize_from_wout_100(error_status, filename)
  USE stelltools, ONLY: initialize_from_wout__wrap => initialize_from_wout
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  CHARACTER(len=*), INTENT(IN) :: filename
  error_status = 0
  CALL initialize_from_wout__wrap(error_status, FILENAME=TRIM(filename))
END SUBROUTINE initialize_from_wout_100 

SUBROUTINE initialize_from_wout_111(error_status, filename, nu, nv)
  USE stelltools, ONLY: initialize_from_wout__wrap => initialize_from_wout
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  CHARACTER(len=*), INTENT(IN) :: filename
  INTEGER, INTENT(IN) :: nu, nv
  error_status = 0
  CALL initialize_from_wout__wrap(error_status, FILENAME=TRIM(filename), NU=nu, NV=nv)
END SUBROUTINE initialize_from_wout_111 

SUBROUTINE initialize_from_wout_011(error_status, nu, nv)
  USE stelltools, ONLY: initialize_from_wout__wrap => initialize_from_wout
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  INTEGER, INTENT(IN) :: nu, nv
  error_status = 0
  CALL initialize_from_wout__wrap(error_status, NU=nu, NV=nv)
END SUBROUTINE initialize_from_wout_011 


SUBROUTINE initialize_splines (error_status)
  USE stelltools, ONLY: initialize_splines__wrap => initialize_splines
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  error_status = 0
  CALL initialize_splines__wrap(error_status)
END SUBROUTINE initialize_splines


SUBROUTINE initialize_splines_rz (error_status)
  USE stelltools, ONLY: initialize_splines_rz__wrap => initialize_splines_rz
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  error_status = 0
  CALL initialize_splines_rz__wrap(error_status)
END SUBROUTINE initialize_splines_rz


SUBROUTINE initialize_splines_rzderiv (error_status)
  USE stelltools, ONLY: initialize_splines_rzderiv__wrap => initialize_splines_rzderiv
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  error_status = 0
  CALL initialize_splines_rzderiv__wrap(error_status)
END SUBROUTINE initialize_splines_rzderiv


SUBROUTINE initialize_splines_b (error_status)
  USE stelltools, ONLY: initialize_splines_b__wrap => initialize_splines_b
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  error_status = 0
  CALL initialize_splines_b__wrap(error_status)
END SUBROUTINE initialize_splines_b


SUBROUTINE initialize_splines_lambda (error_status)
  USE stelltools, ONLY: initialize_splines_lambda__wrap => initialize_splines_lambda
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  error_status = 0
  CALL initialize_splines_lambda__wrap(error_status)
END SUBROUTINE initialize_splines_lambda


SUBROUTINE initialize_splines_jacobian (error_status)
  USE stelltools, ONLY: initialize_splines_jacobian__wrap => initialize_splines_jacobian
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  error_status = 0
  CALL initialize_splines_jacobian__wrap(error_status)
END SUBROUTINE initialize_splines_jacobian


SUBROUTINE initialize_splines_fsa (error_status)
  USE stelltools, ONLY: initialize_splines_fsa__wrap => initialize_splines_fsa
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  error_status = 0
  CALL initialize_splines_fsa__wrap(error_status)
END SUBROUTINE initialize_splines_fsa


SUBROUTINE initialize_splines_susceptance (error_status)
  USE stelltools, ONLY: initialize_splines_susceptance__wrap => initialize_splines_susceptance
  IMPLICIT NONE
  INTEGER, INTENT(OUT) :: error_status
  error_status = 0
  CALL initialize_splines_susceptance__wrap(error_status)
END SUBROUTINE initialize_splines_susceptance


! ------------------------------------------------------------------------------
! Begin INTERFACE flx_from_cyl.
! ------------------------------------------------------------------------------

SUBROUTINE flx_from_cyl_0(value_in, value_out, error_status)
  USE stelltools, ONLY: flx_from_cyl__wrap => flx_from_cyl
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL flx_from_cyl__wrap(value_in, value_out, error_status)
END SUBROUTINE flx_from_cyl_0

SUBROUTINE flx_from_cyl_1(value_in, value_out, error_status, guess)
  USE stelltools, ONLY: flx_from_cyl__wrap => flx_from_cyl
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  DOUBLE PRECISION, INTENT(in) ::  guess
  error_status = 0
  CALL flx_from_cyl__wrap(value_in, value_out, error_status, guess)
END SUBROUTINE flx_from_cyl_1


SUBROUTINE cyl_from_flx(value_in, value_out, error_status)
  USE stelltools, ONLY: cyl_from_flx__wrap => cyl_from_flx
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL cyl_from_flx__wrap(value_in, value_out, error_status)
END SUBROUTINE cyl_from_flx


SUBROUTINE cyl_from_car(value_in, value_out, error_status)
  USE stelltools, ONLY: cyl_from_car__wrap => cyl_from_car
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL cyl_from_car__wrap(value_in, value_out, error_status)
END SUBROUTINE cyl_from_car


SUBROUTINE car_from_cyl(value_in, value_out, error_status)
  USE stelltools, ONLY: car_from_cyl__wrap => car_from_cyl
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL car_from_cyl__wrap(value_in, value_out, error_status)
END SUBROUTINE car_from_cyl


SUBROUTINE modb_from_flx(value_in, value_out, error_status)
  USE stelltools, ONLY: modb_from_flx__wrap => modb_from_flx
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL modb_from_flx__wrap(value_in, value_out, error_status)
END SUBROUTINE modb_from_flx


SUBROUTINE b_flx_from_flx(value_in, value_out, error_status)
  USE stelltools, ONLY: b_flx_from_flx__wrap => b_flx_from_flx
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL b_flx_from_flx__wrap(value_in, value_out, error_status)
END SUBROUTINE b_flx_from_flx


SUBROUTINE b_cyl_from_flx(value_in, value_out, error_status)
  USE stelltools, ONLY: b_cyl_from_flx__wrap => b_cyl_from_flx
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL b_cyl_from_flx__wrap(value_in, value_out, error_status)
END SUBROUTINE b_cyl_from_flx


SUBROUTINE b_car_from_flx(value_in, value_out, error_status)
  USE stelltools, ONLY: b_car_from_flx__wrap => b_car_from_flx
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL b_car_from_flx__wrap(value_in, value_out, error_status)
END SUBROUTINE b_car_from_flx


SUBROUTINE b_cyl_from_cyl(value_in, value_out, error_status)
  USE stelltools, ONLY: b_cyl_from_cyl__wrap => b_cyl_from_cyl
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL b_cyl_from_cyl__wrap(value_in, value_out, error_status)
END SUBROUTINE b_cyl_from_cyl


SUBROUTINE b_car_from_cyl(value_in, value_out, error_status)
  USE stelltools, ONLY: b_car_from_cyl__wrap => b_car_from_cyl
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL b_car_from_cyl__wrap(value_in, value_out, error_status)
END SUBROUTINE b_car_from_cyl


SUBROUTINE vp_from_s(value_in, value_out, error_status)
  USE stelltools, ONLY: vp_from_s__wrap => vp_from_s
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in
  DOUBLE PRECISION, INTENT(out) ::  value_out
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL vp_from_s__wrap(value_in, value_out, error_status)
END SUBROUTINE vp_from_s


SUBROUTINE surf_area_from_s(value_in, value_out, error_status)
  USE stelltools, ONLY: surf_area_from_s__wrap => surf_area_from_s
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in
  DOUBLE PRECISION, INTENT(out) ::  value_out
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL surf_area_from_s__wrap(value_in, value_out, error_status)
END SUBROUTINE surf_area_from_s


SUBROUTINE fsa_modb_from_s(value_in, value_out, error_status)
  USE stelltools, ONLY: fsa_modb_from_s__wrap => fsa_modb_from_s
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in
  DOUBLE PRECISION, INTENT(out) ::  value_out
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL fsa_modb_from_s__wrap(value_in, value_out, error_status)
END SUBROUTINE fsa_modb_from_s


SUBROUTINE fsa_b2_from_s(value_in, value_out, error_status)
  USE stelltools, ONLY: fsa_b2_from_s__wrap => fsa_b2_from_s
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in
  DOUBLE PRECISION, INTENT(out) ::  value_out
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL fsa_b2_from_s__wrap(value_in, value_out, error_status)
END SUBROUTINE fsa_b2_from_s


SUBROUTINE fsa_gradrho_from_s(value_in, value_out, error_status)
  USE stelltools, ONLY: fsa_gradrho_from_s__wrap => fsa_gradrho_from_s
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in
  DOUBLE PRECISION, INTENT(out) ::  value_out
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL fsa_gradrho_from_s__wrap(value_in, value_out, error_status)
END SUBROUTINE fsa_gradrho_from_s


SUBROUTINE fsa_gradrho2_from_s(value_in, value_out, error_status)
  USE stelltools, ONLY: fsa_gradrho2_from_s__wrap => fsa_gradrho2_from_s
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in
  DOUBLE PRECISION, INTENT(out) ::  value_out
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL fsa_gradrho2_from_s__wrap(value_in, value_out, error_status)
END SUBROUTINE fsa_gradrho2_from_s


SUBROUTINE fsa_b2overgradrho_from_s(value_in, value_out, error_status)
  USE stelltools, ONLY: fsa_b2overgradrho_from_s__wrap => fsa_b2overgradrho_from_s
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in
  DOUBLE PRECISION, INTENT(out) ::  value_out
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL fsa_b2overgradrho_from_s__wrap(value_in, value_out, error_status)
END SUBROUTINE fsa_b2overgradrho_from_s


SUBROUTINE grads_cyl_from_flx(value_in, value_out, error_status)
  USE stelltools, ONLY: grads_cyl_from_flx__wrap => grads_cyl_from_flx
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL grads_cyl_from_flx__wrap(value_in, value_out, error_status)
END SUBROUTINE grads_cyl_from_flx


SUBROUTINE grads_car_from_flx(value_in, value_out, error_status)
  USE stelltools, ONLY: grads_car_from_flx__wrap => grads_car_from_flx
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL grads_car_from_flx__wrap(value_in, value_out, error_status)
END SUBROUTINE grads_car_from_flx


SUBROUTINE gradrho_cyl_from_flx(value_in, value_out, error_status)
  USE stelltools, ONLY: gradrho_cyl_from_flx__wrap => gradrho_cyl_from_flx
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL gradrho_cyl_from_flx__wrap(value_in, value_out, error_status)
END SUBROUTINE gradrho_cyl_from_flx


SUBROUTINE gradrho_car_from_flx(value_in, value_out, error_status)
  USE stelltools, ONLY: gradrho_car_from_flx__wrap => gradrho_car_from_flx
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out(3)
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL gradrho_car_from_flx__wrap(value_in, value_out, error_status)
END SUBROUTINE gradrho_car_from_flx


SUBROUTINE jacobian_from_flx(value_in, value_out, error_status)
  USE stelltools, ONLY: jacobian_from_flx__wrap => jacobian_from_flx
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(in) ::  value_in(3)
  DOUBLE PRECISION, INTENT(out) ::  value_out
  INTEGER, INTENT(out) ::  error_status
  error_status = 0
  CALL jacobian_from_flx__wrap(value_in, value_out, error_status)
END SUBROUTINE jacobian_from_flx
