# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:

"""
# ------------------------------------------------------------------------------

import logging
import numpy as np
import numpy.matlib
from scipy import interpolate


# The first thing I want to do is figure out how to convert from
# the fourier representation to real space.
def sfunct(theta,zeta,zmns,xm,xn):
    """
    CFUNCT(theta,zeta,zmns,xm,xn) Cosine Fourier Transform
    This function returns the cosine Fourier tranform of a value for a
    given theta and zeta (coordinates over the transform).  It assumes a
    form of the transform where:
    f=\sum_{m=0,M}\sum_{n=-N,N} rbc(m,n)*cos(m*theta-n*nfp*zeta)
    the VMEC Fourier transform.

    f=CFUNCT(theta,zeta,zmns,xm,xn)
    Inputs
    theta:    poloidal coordinate (0,2pi)
    zeta:     toroidal coordinate (0,2pi)
    zmns:     Array of Fouier Coefficients (mn,ns)
    xm:       Poloidal mode number array (mn)
    xn:       Toroidal mode number array (mn)


    Exmaple Usage
         theta=0:2*pi/36:2*pi
         zeta=0:2*pi/36:2*pi
         data=read_vmec('wout.test')
         z=sfunct(theta,zeta,data.zmns,data.xm,data.xn)

    Maintained by: Samuel Lazerson (lazerson@pppl.gov)
    Version:       2.00

    2011-10-17 - SAL:
      Vectorized Version for increased speed.
    2015-08-21 - Novimir:
      Ported to Python.
    """

    if np.isscalar(theta): theta = [theta]
    if np.isscalar(zeta): zeta = [zeta]

    xm = np.array([xm])
    xn = np.array([xn])
    theta = np.array([theta])
    zeta = np.array([zeta])

    ns=zmns.shape[0]
    lt=theta.size
    lz=zeta.size

    # Create mode x angle arrays
    mt=np.dot(xm.T, theta)
    nz=np.dot(xn.T, zeta)

    # Create Trig Arrays
    cosmt=np.cos(mt)
    sinmt=np.sin(mt)
    cosnz=np.cos(nz)
    sinnz=np.sin(nz)

    # Calcualte the transform
    f=np.zeros((ns,lt,lz))
    for kk in range(0,ns):
        zmn = np.matlib.repmat(zmns[kk, :], lt, 1)

        f[kk, :, :] = (np.dot((zmn.T*sinmt).T, cosnz)
                       - np.dot((zmn.T*cosmt).T, sinnz))

    return f


def cfunct(theta,zeta,rmnc,xm,xn):
    """
    CFUNCT(theta,zeta,rmnc,xm,xn) Cosine Fourier Transform
    This function returns the cosine Fourier tranform of a value for a
    given theta and zeta (coordinates over the transform).  It assumes a
    form of the transform where:
    f=\sum_{m=0,M}\sum_{n=-N,N} rbc(m,n)*cos(m*theta-n*nfp*zeta)
    the VMEC Fourier transform.

    f=CFUNCT(theta,zeta,rmnc,xm,xn)
    Inputs
    theta:    poloidal coordinate (0,2pi)
    zeta:     toroidal coordinate (0,2pi)
    rmnc:     Array of Fouier Coefficients (mn,ns)
    xm:       Poloidal mode number array (mn)
    xn:       Toroidal mode number array (mn)


    Exmaple Usage
         theta=0:2*pi/36:2*pi
         zeta=0:2*pi/36:2*pi
         data=read_vmec('wout.test')
         r=cfunct(theta,zeta,data.rmnc,data.xm,data.xn)

    Maintained by: Samuel Lazerson (lazerson@pppl.gov)
    Version:       2.00

    2011-10-17 - SAL:
      Vectorized Version for increased speed.
    2015-08-21 - Novimir:
      Ported to Python.
    """

    if np.isscalar(theta): theta = [theta]
    if np.isscalar(zeta): zeta = [zeta]

    xm = np.array([xm])
    xn = np.array([xn])
    theta = np.array([theta])
    zeta = np.array([zeta])

    ns=rmnc.shape[0]
    lt=theta.size
    lz=zeta.size

    # Create mode x angle arrays
    mt=np.dot(xm.T, theta)
    nz=np.dot(xn.T, zeta)

    # Create Trig Arrays
    cosmt=np.cos(mt)
    sinmt=np.sin(mt)
    cosnz=np.cos(nz)
    sinnz=np.sin(nz)

    # Calcualte the transform
    f=np.zeros((ns,lt,lz))
    for kk in range(0,ns):
        rmn = np.matlib.repmat(rmnc[kk, :], lt, 1)

        f[kk, :, :] = (np.dot((rmn.T*cosmt).T, cosnz)
                       - np.dot((rmn.T*sinmt).T, sinnz))

    return f


def cyl(theta, zeta, wout):
    z = sfunct(theta, zeta, wout['zmns'], wout['xm'], wout['xn'])
    R = cfunct(theta, zeta, wout['rmnc'], wout['xm'], wout['xn'])
    zeta_array = np.repeat([np.repeat([zeta], 3, axis=0)],99,axis=0)
    return [R,zeta_array,z]


def halfToFullMesh(value, ns):
    if value.ndim == 1:
        return halfToFullMeshScalar(value, ns)
    else:
        return halfToFullMeshArray(value, ns)


def halfToFullMeshScalar(value, ns):
    """
    Convert a Scalar given on the half mesh to the full mesh.
    """

    temp = np.zeros(ns)

    # First Index (note indexing on vectors is 2:ns when VMEC outputs)
    temp[0] = 1.5 * value[1] - 0.5 * value[2]

    # Average
    for ii in range(1, ns-1):
        temp[ii]    = 0.5 * (value[ii] + value[ii+1] )

    # Last Index (note indexing on vectors is 2:ns when VMEC outputs)
    temp[-1] = 2.0 * value[-2] - value[-3]

    return temp


def halfToFullMeshArray(value, ns):
    """
    Convert a array given on the half mesh to the full mesh.
    """

    # Make a copy so we don't overwrite the original halfmesh variable.
    value = value.copy()

    # First Index (note indexing on vectors is 2:ns when VMEC outputs)
    value[0,:]=     1.5 *     value[1,:] - 0.5 *     value[2,:]

    # Average
    for ii in range(1, ns-1):
        value[ii,:]    = 0.5 * (value[ii,:] +     value[ii+1, :] )

    # Last Index (note indexing on vectors is 2:ns when VMEC outputs)
    value[-1,:] = 2.0 *     value[-2,:] -     value[-3,:]

    return value


def findFluxSurfaceAverage(
        function
        ,s_index
        ,wout
        ,num_theta=100
        ,num_zeta=100
        ,num_periods=1):
    """
    Calculate a flux surface average for a function.
    The function must be callable as function(s, theta, zeta).
    """

    # For now just sum over everything.
    # This would be faster though if I only did the calculation
    # on a half field period.

    # For now I am just going to use an existing surface number: s_index
    s = (s_index/(wout['ns']-1))

    theta = np.linspace(0, 2*np.pi, num_theta)
    zeta = np.linspace(0, 2*np.pi/num_periods, num_zeta)

    jacobian_mesh = np.zeros([num_theta, num_zeta])
    funct_mesh = np.zeros([num_theta, num_zeta])

    # First convert the gmnc array to the full mesh.
    gmnc_full = halfToFullMesh(wout['gmnc'], wout['ns'])

    jacobian_mesh = cfunct(theta, zeta, gmnc_full, wout['xm'], wout['xn'])


    # Well if the funciton can take arrays of theta and zeta this could be
    # a lot faster.
    for ii_theta in range(0,num_theta):
        for ii_zeta in range(0,num_zeta):
            point_flux = np.array([s, theta[ii_theta], zeta[ii_zeta]])
            funct_mesh[ii_theta, ii_zeta] = function(point_flux)


    # Now calculate the sums.
    fj_sum = np.sum(funct_mesh * jacobian_mesh[s_index, :, :])
    j_sum = np.sum(jacobian_mesh[s_index, :, :])


    return fj_sum/j_sum


def reffProfile(wout):
    """
    Return a profile of Reff values at the VMEC surfaces.

    Reff is defined as:
    The radius of a circle with equivalent area to the toroidally
    averaged poloidal cross sectional area of a flux surface.
    """
    reff = np.zeros(wout['ns'])

    for ii in range(0,wout['ns']):
        reff[ii] = np.sqrt(sum(wout['rmnc'][ii,:]*wout['zmns'][ii,:]*wout['xm']))

    return reff


def sProfile(wout):
    """
    Return the normalized effective radius (s) at the VMEC surfaces

    s is defined as:
    phi/phi_edge where phi is the enclosed toroidal flux.
    """
    s = wout['phi']/wout['phi'][-1]

    return s


def reff(wout, s):
    """
    Return Reff for a particular value of s.

    Reff is defined as:
      The radius of a circle with equivalent area to the toroidally
      averaged poloidal cross sectional area of a flux surface.
    """
    reffprofile = reffProfile(wout)
    sprofile = sProfile(wout)

    # For now use cubic spline interpolation: k=3.
    spline = interpolate.InterpolatedUnivariateSpline(sprofile, reffprofile)

    return spline(s)


def reffPsi(wout, s):
    """
    Return a value of reff based on the normalized flux.
    
    Reff_psi is defined as:
      rho*Aminor_p
    Which is equivilent to:
      sqrt(psi/psi_edge)*Aminor_p
    """

    return np.sqrt(s)*wout['Aminor_p']


def calculateJacobianMatrix(function, point_car, epsilon=None):
    """
    Calculate the gradient of a function based on simple 
    finite differences.
    
    Programming notes:
      With stelltools.cyl_to_flx this produces sensible
      results with epsilon > 1e-10.
    """

    # Set the default epsilon.
    if epsilon is None: epsilon = 1e-6

    value = function(point_car)
    value_dx = function(point_car + np.array([epsilon, 0, 0]))
    value_dy = function(point_car + np.array([0, epsilon, 0]))
    value_dz = function(point_car + np.array([0, 0, epsilon]))

    # Transpose so that the idicies have the usual J_ij meaning.
    jm = np.array([value_dx-value
                   ,value_dy-value
                   ,value_dz-value]).T

    #print('point_car', point_car)
    #print('value   ', value)
    #print('value_dx', value_dx)
    #print('value_dy', value_dy)
    #print('value_dz', value_dz)
    #print('jm: ', jm)

    jm = jm/epsilon

    return jm, value
