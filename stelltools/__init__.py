# -*- coding: utf-8 -*-
"""
author
======
  Novimir Antoniuk Pablant
  - npablant@pppl.gov
  - novimir.pablant@amicitas.com

purpose
=======
  Provides a set of tools to work with VMEC equilibria.
  - Conversion between flux coordinates and real coordinates.
  - Extraction of gradient and magnetic field values.
  - Calculation of flux surface averaged quantities.
  
  This module is a python wrapper of the fortran module:
  MIR_TOOLS, which is available as part of the STELLOPT
  distribution.  Documentation is maintaned within the
  FORTRAN source code.

"""


import numpy as np
import logging

from stelltools import stelltools_fortran
from stelltools import util

# Some module level variables used to avoid unnessary re-initialization.
# This should probably eventually be handled within the fortran code.
m_cache = {}
m_cache['initialized'] = False
m_cache['filename'] = None
m_cache['nu'] = None
m_cache['nv'] = None

def _handle_error_status(error_status):
    """
    This will handle standard error codes from the stelltools module.
    """
    if error_status == 0:
        return
    if error_status == -2:
        raise DomainError()
    else:
        raise Exception('Error code: {}'.format(error_status))


# Python interfaces as part of a workaround for f2py not handling optional
# arguments or overloading.

def isInitialized(filename=None, nu=None, nv=None):
    """
    Check to see if stelltools is already initialized with the given parameters.
    """
    global m_cache
    initialized = (
        m_cache['initialized'] == True
        and m_cache['filename'] == filename
        and m_cache['nu'] == nu
        and m_cache['nv'] == nv)
    return initialized


def setInitialized(filename=None, nu=None, nv=None):
    """
    Save the initialization parameters in the module cache.
    """
    global m_cache
    m_cache['initialized'] = True
    m_cache['filename'] = filename
    m_cache['nu'] = nu
    m_cache['nv'] = nv
    

def initialize_from_wout(filename=None, nu=None, nv=None):
    log = logging.getLogger('stelltools')
    
    if isInitialized(filename, nu, nv):
        return
    
    log.debug('Initializing stelltools with {}'.format(filename))
    
    if filename is None and nu is None and nv is None:
        error_status = stelltools_fortran.initialize_from_wout_000()
    elif filename is not None and nu is None and nv is None:
        error_status = stelltools_fortran.initialize_from_wout_100(filename)
    elif filename is None and nu is not None and nv is not None:
        error_status = stelltools_fortran.initialize_from_wout_011(nu, nv)
    elif filename is not None and nu is not None and nv is not None:
        error_status = stelltools_fortran.initialize_from_wout_111(filename, nu, nv)
    else:
        raise Exception('Both or none of nu, nv must be given as inputs.')
    _handle_error_status(error_status)

    setInitialized(filename, nu, nv)


def flx_from_cyl(value_in, guess=None):
    """
    Convert a point in cylindrical coordinates (r, phi, z) to a point in 
    flux coordinates (s, u, phi).
    """
    if guess is None:
        value_out, error_status = stelltools_fortran.flx_from_cyl_0(value_in)
    else:
        value_out, error_status = stelltools_fortran.flx_from_cyl_1(value_in, guess)
    _handle_error_status(error_status)
        
    return value_out


# Simple wrapping to expose these functions in the python module.

def read_wout_file(value_in):
    error_status = stelltools_fortran.read_wout_file(value_in)
    _handle_error_status(error_status)


def initialize_splines():
    error_status = stelltools_fortran.initialize_splines()
    _handle_error_status(error_status)


def initialize_splines_rz():
    error_status = stelltools_fortran.initialize_splines_rz()
    _handle_error_status(error_status)


def initialize_splines_rzderiv():
    error_status = stelltools_fortran.initialize_splines_rzderiv()
    _handle_error_status(error_status)


def initialize_splines_b():
    error_status = stelltools_fortran.initialize_splines_b()
    _handle_error_status(error_status)


def initialize_splines_modb():
    error_status = stelltools_fortran.initialize_splines_modb()
    _handle_error_status(error_status)


def initialize_splines_jacobian():
    error_status = stelltools_fortran.initialize_splines_jacobian()
    _handle_error_status(error_status)


def initialize_splines_fsa():
    error_status = stelltools_fortran.initialize_splines_fsa()
    _handle_error_status(error_status)


def initialize_splines_lambda():
    error_status = stelltools_fortran.initialize_splines_lambda()
    _handle_error_status(error_status)


def initialize_splines_susceptance():
    error_status = stelltools_fortran.initialize_splines_susceptance()
    _handle_error_status(error_status)


def cyl_from_car(value_in):
    value_out, error_status = stelltools_fortran.cyl_from_car(value_in)
    _handle_error_status(error_status)
    return value_out


def car_from_cyl(value_in):
    value_out, error_status = stelltools_fortran.car_from_cyl(value_in)
    _handle_error_status(error_status)
    return value_out


def cyl_from_flx(value_in):
    """
    Convert a point in flux coordinates (s, u, phi) to a point in 
    cylindrical coordinates(r, phi, z).
    """
    value_out, error_status = stelltools_fortran.cyl_from_flx(value_in)
    _handle_error_status(error_status)
    return value_out


def modb_from_flx(value_in):
    value_out, error_status = stelltools_fortran.modb_from_flx(value_in)
    _handle_error_status(error_status)
    return value_out


def b_flx_from_flx(value_in):
    value_out, error_status = stelltools_fortran.b_flx_from_flx(value_in)
    _handle_error_status(error_status)
    return value_out


def b_cyl_from_flx(value_in):
    value_out, error_status = stelltools_fortran.b_cyl_from_flx(value_in)
    _handle_error_status(error_status)
    return value_out


def b_car_from_flx(value_in):
    value_out, error_status = stelltools_fortran.b_car_from_flx(value_in)
    _handle_error_status(error_status)
    return value_out


def b_cyl_from_cyl(value_in):
    value_out, error_status = stelltools_fortran.b_cyl_from_cyl(value_in)
    _handle_error_status(error_status)
    return value_out


def b_car_from_cyl(value_in):
    value_out, error_status = stelltools_fortran.b_car_from_cyl(value_in)
    _handle_error_status(error_status)
    return value_out


def vp_from_s(value_in):
    value_out, error_status = stelltools_fortran.vp_from_s(value_in)
    _handle_error_status(error_status)
    return value_out


def surf_area_from_s(value_in):
    value_out, error_status = stelltools_fortran.surf_area_from_s(value_in)
    _handle_error_status(error_status)
    return value_out


def fsa_modb_from_s(value_in):
    value_out, error_status = stelltools_fortran.fsa_modb_from_s(value_in)
    _handle_error_status(error_status)
    return value_out


def fsa_b2_from_s(value_in):
    value_out, error_status = stelltools_fortran.fsa_b2_from_s(value_in)
    _handle_error_status(error_status)
    return value_out


def fsa_gradrho_from_s(value_in):
    value_out, error_status = stelltools_fortran.fsa_gradrho_from_s(value_in)
    _handle_error_status(error_status)
    return value_out


def fsa_gradrho2_from_s(value_in):
    value_out, error_status = stelltools_fortran.fsa_gradrho2_from_s(value_in)
    _handle_error_status(error_status)
    return value_out


def fsa_b2overgradrho_from_s(value_in):
    value_out, error_status = stelltools_fortran.fsa_b2overgradrho_from_s(value_in)
    _handle_error_status(error_status)
    return value_out


def grads_cyl_from_flx(value_in):
    value_out, error_status = stelltools_fortran.grads_cyl_from_flx(value_in)
    _handle_error_status(error_status)
    return value_out


def grads_car_from_flx(value_in):
    value_out, error_status = stelltools_fortran.grads_car_from_flx(value_in)
    _handle_error_status(error_status)
    return value_out


def gradrho_cyl_from_flx(value_in):
    value_out, error_status = stelltools_fortran.gradrho_cyl_from_flx(value_in)
    _handle_error_status(error_status)
    return value_out


def gradrho_car_from_flx(value_in):
    value_out, error_status = stelltools_fortran.gradrho_car_from_flx(value_in)
    _handle_error_status(error_status)
    return value_out


def jacobian_from_flx(value_in):
    value_out, error_status = stelltools_fortran.jacobian_from_flx(value_in)
    _handle_error_status(error_status)
    return value_out

# Pure Python routines


def flx_from_car(point_car):
    """
    Convert a point in cartesian coordinates to a point in flux coordinates.
    """
    return flx_from_cyl(cyl_from_car(point_car))


def car_from_flx(point_flx):
    """
    Convert a point in flux coordinates to a point in cartesian coordinates.
    """
    return car_from_cyl(cyl_from_flx(point_flx))


# Define a set of routines for additional calculations.


def finitediff_grads_from_car(point_car):
    """
    Calculate the gradient of s (grad(s)) at a point in real space
    (cartesian) coordinates using a finite different method.
    """
    jm, point_flx = util.calculateJacobianMatrix(flx_from_car, point_car)
    return jm[0,:]


def finitediff_gradrho_from_car(point_car):
    """
    Calculate the gradient of rho (del(rho)) at a point in real space
    (cartesian) coordinates using a finite different method.
    """

    jm, point_flx = util.calculateJacobianMatrix(flx_from_car, point_car)

    if point_flx[0] < 1e-8:
        print('')
        print(point_car)
        print(point_flx)
        raise Exception('A finite different flux gradient is not defined at s=0')

    grad_s = jm[0,:]
    grad_rho = grad_s*0.5/np.sqrt(point_flx[0])

    return grad_rho


class DomainError(Exception):
    """
    A general exception for out of domain type errors.
    """
    pass
