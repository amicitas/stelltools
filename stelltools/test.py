
# Just a quick temporary testing script.
#
# For now I am just doing all the tests inline to make ipython inspection easier.

import numpy as np
import unittest

test = unittest.TestCase()

# initialize stelltools.
from mirfusion.vmec import mirtools
from mirfusion.vmec import util
from mirfusion import vmec

flag_plot = False
if flag_plot:
    from mirutil.plot import mirplot

woutfile = '/u/npablant/data/w7x/vmec/wout_w7x.1000_1000_1000_1000_+0390_+0390.01.00s.nc'
wout = vmec.readWout(woutfile)

mirtools.initialize_from_vmec(woutfile)

# Choose a specific index value of s for our tests.
# This allows me to do easier comparisons to non-splined/non-interpolated values.
s_vmec = wout['phi']/wout['phi'][-1]
s_index = 40
s_adjust = 0.0

start_point_flx = np.array([s_vmec[s_index]+s_adjust, np.pi/8.0, np.pi/8.0])
start_point_cyl = mirtools.cyl_from_flx(start_point_flx)
start_point_car = mirtools.car_from_cyl(start_point_cyl)

#start_point_cyl = np.array([5.8, np.pi/8.0, 0.0])



# First do a basic test going between cylindrical and flux coordinates..
st_flx = mirtools.flx_from_cyl(start_point_cyl)
st_cyl = mirtools.cyl_from_flx(st_flx)

print('{:10s}: {}'.format(
    'start_flx:'
    ,', '.join(['{:10.6f}'.format(x) for x in start_point_flx])))
print('{:10s}: {}'.format(
    'start_cyl:'
    ,', '.join(['{:10.6f}'.format(x) for x in start_point_cyl])))
print('{:10s}: {}'.format(
    'st_flx:'
    ,', '.join(['{:10.6f}'.format(x) for x in st_flx])))
print('{:10s}: {}'.format(
    'st_cyl:'
    ,', '.join(['{:10.6f}'.format(x) for x in st_cyl])))


# Check the magnetic field for consistency.
st_b_car = mirtools.b_car_from_cyl(start_point_cyl)
st_b_cyl = mirtools.b_cyl_from_cyl(start_point_cyl)
st_b_flx = mirtools.b_flx_from_flx(st_flx)
st_modb = mirtools.modb_from_flx(st_flx)

st_modb_car = np.linalg.norm(st_b_car)
st_modb_cyl = np.linalg.norm(st_b_cyl)

print('st_modb:     {:10.6f}'.format(st_modb))
print('st_modb_cyl: {:10.6f}'.format(st_modb_cyl))
print('st_modb_car: {:10.6f}'.format(st_modb_car))

test.assertAlmostEqual(st_modb, st_modb_cyl, places=3)
test.assertAlmostEqual(st_modb, st_modb_car, places=3)


# ------------------------------------------------------------------------------
# Check grad(s) and grad(rho).

# Calculated by mir_tools
st_grads_cyl = mirtools.grads_cyl_from_flx(st_flx)
st_grads_car = mirtools.grads_car_from_flx(st_flx)
st_gradrho_cyl = mirtools.gradrho_cyl_from_flx(st_flx)
st_gradrho_car = mirtools.gradrho_car_from_flx(st_flx)

# Finite different calculation
fd_jm_cyl, _dummy = util.calculateJacobianMatrix(mirtools.flx_from_cyl, start_point_cyl)
fd_grads_cyl = fd_jm_cyl[0,:]
fd_grads_cyl[1] /= st_cyl[0]
fd_gradrho_cyl = fd_grads_cyl.copy() * 0.5 / np.sqrt(st_flx[0])

fd_jm_car, _dummy = util.calculateJacobianMatrix(mirtools.flx_from_car, start_point_car)
fd_grads_car = fd_jm_car[0,:]
fd_gradrho_car = fd_grads_car.copy() * 0.5 / np.sqrt(st_flx[0])


print('{:10s}: {}'.format(
    'st_grads_cyl:'
    ,', '.join(['{:10.6f}'.format(x) for x in st_grads_cyl])))
print('{:10s}: {}'.format(
    'fd_grads_cyl:'
    ,', '.join(['{:10.6f}'.format(x) for x in fd_grads_cyl])))
print('{:10s}: {}'.format(
    'st_grads_car:'
    ,', '.join(['{:10.6f}'.format(x) for x in st_grads_car])))
print('{:10s}: {}'.format(
    'fd_grads_car:'
    ,', '.join(['{:10.6f}'.format(x) for x in fd_grads_car])))
print('')
print('{:10s}: {}'.format(
    'st_gradrho_cyl:'
    ,', '.join(['{:10.6f}'.format(x) for x in st_gradrho_cyl])))
print('{:10s}: {}'.format(
    'fd_gradrho_cyl:'
    ,', '.join(['{:10.6f}'.format(x) for x in fd_gradrho_cyl])))
print('{:10s}: {}'.format(
    'st_gradrho_car:'
    ,', '.join(['{:10.6f}'.format(x) for x in st_gradrho_car])))
print('{:10s}: {}'.format(
    'fd_gradrho_car:'
    ,', '.join(['{:10.6f}'.format(x) for x in fd_gradrho_car])))


rho_array = np.linspace(0.0, 0.3, 1000)
st_gradrho_array = np.zeros([3, 1000])
st_grads_array = np.zeros([3, 1000])
temp_flx = st_flx.copy()
for ii, rho in enumerate(rho_array):
    temp_flx[0] = rho**2
    st_gradrho_array[:,ii] = mirtools.gradrho_car_from_flx(temp_flx)
    st_grads_array[:,ii] = mirtools.grads_car_from_flx(temp_flx)

if flag_plot:
    plotobj = mirplot.MirPlot()
    plotobj.append({
        'name':0
         ,'x':rho_array
         ,'y':st_gradrho_array[0,:]
         ,'label':'gradrho_car_from_flx'
         ,'xlabel':'rho'
         ,'ylabel':'Drho/Dx'
         ,'legend':True
        })
    plotobj.append({
        'name':0
         ,'x':rho_array[1:]
         ,'y':0.5*st_grads_array[0,1:]/rho_array[1:]
         ,'label':'grads_car_from_flx'
        })
    plotobj.plotToScreen()


# Calculate flux surface averaged values using the finite difference
# gradients.
def mod_gradrho(point_flx):
    start_point_car = mirtools.car_from_flx(point_flx)
    grad_rho = mirtools.finitediff_gradrho_from_car(start_point_car)
    mod_grad_rho = np.linalg.norm(grad_rho)
    #print('point_flx: {}, |grad(rho)|: {:10.4f}'.format(
    #    ','.join(['{:10.6f}'.format(x) for x in point_flx])
    #    ,mod_grad_rho))

    # Look for obvious errors in the gradient and stop for debugging.
    if mod_grad_rho > 10:

        print('point_flx = np.array([{}])'.format(
        ','.join(['{:16.12f}'.format(x) for x in point_flx])))
        print('point_car = np.array([{}])'.format(
        ','.join(['{:20.16f}'.format(x) for x in start_point_car])))
        print('grad_rho = np.array([{}])'.format(
        ','.join(['{:16.12f}'.format(x) for x in grad_rho])))

        from IPython import embed
        embed()

    return mod_grad_rho



fd_fsa_gradrho = util.findFluxSurfaceAverage(mod_gradrho, s_index, wout, num_periods=5)
st_fsa_gradrho = mirtools.fsa_gradrho_from_s(st_flx[0])

fd_fsa_modb = util.findFluxSurfaceAverage(mirtools.modb_from_flx, s_index, wout, num_periods=5)
st_fsa_modb = mirtools.fsa_modb_from_s(st_flx[0])


print('st_fsa_gradrho: {:10.6f}, fd_fsa_gradrho: {:10.6f}'.format(st_fsa_gradrho, fd_fsa_gradrho))
print('st_fsa_modb:    {:10.6f}, fd_fsa_modb:    {:10.6f}'.format(st_fsa_modb, fd_fsa_modb))



# Now do some comparisons beween stel_tools and ajax.
# Initialize ajax.
#
# WARNING:
#   I have archived my ajax library so this comparisons
#   not possible to do at the moment.  The code still
#   exists under archive/ajaxtools if these comparisons
#   are needed in the future.
flag_compare_to_ajax = False
if flag_compare_to_ajax:
    from mirfusion.vmec import pyvmec

    pyvmec.read_wout(woutfile)
    pyvmec.initialize_ajax()

    ajax_cyl = pyvmec.flx2cyl(st_flx)
    ajax_flx = pyvmec.cyl2flx(start_point_cyl)
    ajax_b = pyvmec.bField(ajax_flx)

    print('start_point_cyl:', start_point_cyl)
    print('st_cyl:  {}    ajax_cyl:  {}'.format(
         ', '.join(['{:10.6f}'.format(x) for x in st_cyl])
         ,', '.join(['{:10.6f}'.format(x) for x in ajax_cyl])))
    print('st_flx:  {}    ajax_flx:  {}'.format(
         ', '.join(['{:10.6f}'.format(x) for x in st_flx])
         ,', '.join(['{:10.6f}'.format(x) for x in ajax_flx])))
    print('st_b:    {}    ajax_b:    {}'.format(
         ', '.join(['{:10.6f}'.format(x) for x in st_b_car])
         ,', '.join(['{:10.6f}'.format(x) for x in ajax_b['b_car']])))
    print('st_bcyl: {}    ajax_bcyl: {}'.format(
         ', '.join(['{:10.6f}'.format(x) for x in st_b_cyl])
         ,', '.join(['{:10.6f}'.format(x) for x in ajax_b['b_cyl']])))






    # Compare gradients between stel_tools and ajax.
    st_g, _dummy = util.calculateJacobianMatrix(mirtools.flx_from_car, start_point_car)
    ajax_g = pyvmec.gradFlxAtCar(start_point_car)

    print('st_gradFlx: {}    ajax_gradFlx: {}'.format(
         ', '.join(['{:10.6f}'.format(x) for x in st_g[0,:]])
         ,', '.join(['{:10.6f}'.format(x) for x in ajax_g])))
