#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
"""
author
======

Novimir Antoniuk Pablant
- npablant@pppl.gov
- novimir.pablant@amicitas.com


purpose
=======
Build the mirtools module.

"""
import os
import sys
import subprocess
from numpy.distutils.core import Extension
from numpy.distutils.core import setup
from numpy.distutils.command.build import build

class build_preprocess(build):
    
    def run(self):
        
        #from IPython import embed
        #embed()
        
        # Run the source preprocessing for each extension module.
        for ext in self.distribution.ext_modules:
            ext.sources = self.preprocess(ext.sources)

        super().run()
        
    def preprocess(self, sources):
        """
        Preprocess the fortran files.  This requires two steps:
          1. Convert !DEC$ directives to the standad cpp format.
          2. Preprocess the files using cpp.

        Currently this is done using subprocess calls to awk and cpp. A better
        way to do this would be to replace awk with a pure python routine and
        then use the distulils call to the preprocessor:
          distutils.ccompiler.CCompiler.preprocess
        """
        
        print('Preprocessing:')
        pp_path = os.path.join(self.build_temp, 'preprocessed')
        os.makedirs(pp_path, exist_ok=True)
        for ii in range(len(sources)):
            filename_orig = sources[ii]
            filename_new = os.path.join(pp_path, filename_orig)
            filename_c = os.path.join(pp_path, 'temp.c')

            print('Preprocessing: {}'.format(filename_orig))
            os.makedirs(os.path.dirname(filename_new), exist_ok=True)
            subprocess.run(
                'awk -f awk_cdir.awk {} > {}'.format(filename_orig, filename_c)
                ,shell=True
                ,check=True)
            subprocess.run(
                'cpp -traditional-cpp -nostdinc -E -P -C -DNETCDF {} {}'.format(filename_c, filename_new)
                ,shell=True
                ,check=True)
            sources[ii] = filename_new

        return sources


# Read the version number.
exec(open('_version.py').read())

# Read in the README file to use as a long description.
with open('README') as file:
    long_description = file.read()

# Define some system specific parameters.
# There may be a way to generalize this through distutils.
if sys.platform == 'linux':
    library_dirs = ['/usr/lib']
    include_dirs = ['/usr/include']
elif sys.platform == 'darwin':
    library_dirs = ['/opt/local/lib', '/usr/lib']
    include_dirs = ['/opt/local/include']
else:
    raise Exception('Installation is currently only supported on linux and darwin platforms.')

# Options for PyPI
classifiers=[
    "Development Status :: 4 - Beta"
    ,"License :: OSI Approved :: MIT License"
    ,"Topic :: Software Development :: Interpreters"
    ,"Programming Language :: Fortran"
    ,"Programming Language :: Python"
    ]


# The order of the *.f90 sources in these lists is important.
# The order of the *.f sources is not important.
#
# f2py preserves compilation order for *.f90 but not *.f for
# this reason several of the original source files have been
# renamed from .f90 to .f just to allow successuful
# compilation.

sources_ezcdf = [
    'fortran/Ezcdf/handle_err.f90'
    ,'fortran/Ezcdf/ezcdf_opncls.f90'
    ,'fortran/Ezcdf/ezcdf_inqvar.f90'
    ,'fortran/Ezcdf/ezcdf_attrib.f90'
    ,'fortran/Ezcdf/ezcdf_GenGet.f90'
    ,'fortran/Ezcdf/ezcdf_GenPut.f90'
    ,'fortran/Ezcdf/ezcdf.f90'
    ]

sources_pspline = [
    'fortran/Pspline/ezhybrid_init.f90'
    ,'fortran/Pspline/ezhybrid_initx.f90'
    ,'fortran/Pspline/ezlinear_init.f90'
    ,'fortran/Pspline/ezspline_2netcdf.f90'
    ,'fortran/Pspline/ezspline_cdfget3.f90'
    ,'fortran/Pspline/ezspline_cdfput3.f90'
    ,'fortran/Pspline/ezspline_cinterp.f90'
    ,'fortran/Pspline/ezspline_derivative.f90'
    ,'fortran/Pspline/ezspline_free.f90'
    ,'fortran/Pspline/ezspline_gradient.f90'
    ,'fortran/Pspline/ezspline_init.f90'
    ,'fortran/Pspline/ezspline_interp.f90'
    ,'fortran/Pspline/ezspline_isgridregular.f90'
    ,'fortran/Pspline/ezspline_isindomain.f90'
    ,'fortran/Pspline/ezspline_load.f90'
    ,'fortran/Pspline/ezspline_modulo.f90'
    ,'fortran/Pspline/ezspline_save.f90'
    ,'fortran/Pspline/ezspline_setup.f90'
    ,'fortran/Pspline/ezspline_setupx.f90'
    ,'fortran/Pspline/ezspline_type.f90'
    ,'fortran/Pspline/ezspline_obj.f90'
    ,'fortran/Pspline/ezspline.f90'
    
    ,'fortran/Pspline/akherm1.f'
    ,'fortran/Pspline/akherm2.f'
    ,'fortran/Pspline/akherm3.f'
    ,'fortran/Pspline/bcspeval.f'
    ,'fortran/Pspline/bcspgrid.f'
    ,'fortran/Pspline/bcspline.f'
    ,'fortran/Pspline/bcspvec.f'
    ,'fortran/Pspline/bpsplinb.f'
    ,'fortran/Pspline/bpspline.f'
    ,'fortran/Pspline/cspeval.f'
    ,'fortran/Pspline/cspline.f'
    ,'fortran/Pspline/cubsplb.f'
    ,'fortran/Pspline/dnherm1.f'
    ,'fortran/Pspline/dnherm2.f'
    ,'fortran/Pspline/dnherm3.f'
    ,'fortran/Pspline/evbicub.f'
    ,'fortran/Pspline/evintrp2d.f'
    ,'fortran/Pspline/evintrp3d.f'
    ,'fortran/Pspline/evspline.f'
    ,'fortran/Pspline/evtricub.f'
    ,'fortran/Pspline/f2test.f'
    ,'fortran/Pspline/f3test.f'
    ,'fortran/Pspline/genxpkg.f'
    ,'fortran/Pspline/gridbicub.f'
    ,'fortran/Pspline/gridherm1.f'
    ,'fortran/Pspline/gridherm2.f'
    ,'fortran/Pspline/gridherm3.f'
    ,'fortran/Pspline/gridintrp2d.f'
    ,'fortran/Pspline/gridintrp3d.f'
    ,'fortran/Pspline/gridpc1.f'
    ,'fortran/Pspline/gridpc2.f'
    ,'fortran/Pspline/gridpc3.f'
    ,'fortran/Pspline/gridspline.f'
    ,'fortran/Pspline/gridtricub.f'
    ,'fortran/Pspline/herm1ev.f'
    ,'fortran/Pspline/herm2ev.f'
    ,'fortran/Pspline/herm3ev.f'
    ,'fortran/Pspline/ibc_ck.f'
    ,'fortran/Pspline/mkbicub.f'
    ,'fortran/Pspline/mkbicubw.f'
    ,'fortran/Pspline/mkherm1.f'
    ,'fortran/Pspline/mkherm2.f'
    ,'fortran/Pspline/mkherm3.f'
    ,'fortran/Pspline/mkintrp2d.f'
    ,'fortran/Pspline/mkintrp3d.f'
    ,'fortran/Pspline/mkspl2p.f'
    ,'fortran/Pspline/mkspl2pb.f'
    ,'fortran/Pspline/mkspl2z.f'
    ,'fortran/Pspline/mkspl2zb.f'
    ,'fortran/Pspline/mkspl3pb.f'
    ,'fortran/Pspline/mkspl3zb.f'
    ,'fortran/Pspline/mkspline.f'
    ,'fortran/Pspline/mktricub.f'
    ,'fortran/Pspline/mktricubw.f'
    ,'fortran/Pspline/nspline.f'
    ,'fortran/Pspline/pc1ev.f'
    ,'fortran/Pspline/pc2ev.f'
    ,'fortran/Pspline/pc3ev.f'
    ,'fortran/Pspline/psp_tolsum.f'
    ,'fortran/Pspline/pspline.f'
    ,'fortran/Pspline/pspline_calls.f'
    ,'fortran/Pspline/r8akherm1.f'
    ,'fortran/Pspline/r8akherm2.f'
    ,'fortran/Pspline/r8akherm3.f'
    ,'fortran/Pspline/r8bcspeval.f'
    ,'fortran/Pspline/r8bcspgrid.f'
    ,'fortran/Pspline/r8bcspline.f'
    ,'fortran/Pspline/r8bcspvec.f'
    ,'fortran/Pspline/r8bpsplinb.f'
    ,'fortran/Pspline/r8bpspline.f'
    ,'fortran/Pspline/r8cspeval.f'
    ,'fortran/Pspline/r8cspline.f'
    ,'fortran/Pspline/r8cubsplb.f'
    ,'fortran/Pspline/r8dnherm1.f'
    ,'fortran/Pspline/r8dnherm2.f'
    ,'fortran/Pspline/r8dnherm3.f'
    ,'fortran/Pspline/r8evbicub.f'
    ,'fortran/Pspline/r8evintrp2d.f'
    ,'fortran/Pspline/r8evintrp3d.f'
    ,'fortran/Pspline/r8evspline.f'
    ,'fortran/Pspline/r8evtricub.f'
    ,'fortran/Pspline/r8genxpkg.f'
    ,'fortran/Pspline/r8gridbicub.f'
    ,'fortran/Pspline/r8gridherm1.f'
    ,'fortran/Pspline/r8gridherm2.f'
    ,'fortran/Pspline/r8gridherm3.f'
    ,'fortran/Pspline/r8gridintrp2d.f'
    ,'fortran/Pspline/r8gridintrp3d.f'
    ,'fortran/Pspline/r8gridpc1.f'
    ,'fortran/Pspline/r8gridpc2.f'
    ,'fortran/Pspline/r8gridpc3.f'
    ,'fortran/Pspline/r8gridspline.f'
    ,'fortran/Pspline/r8gridtricub.f'
    ,'fortran/Pspline/r8herm1ev.f'
    ,'fortran/Pspline/r8herm2ev.f'
    ,'fortran/Pspline/r8herm3ev.f'
    ,'fortran/Pspline/r8mkbicub.f'
    ,'fortran/Pspline/r8mkbicubw.f'
    ,'fortran/Pspline/r8mkherm1.f'
    ,'fortran/Pspline/r8mkherm2.f'
    ,'fortran/Pspline/r8mkherm3.f'
    ,'fortran/Pspline/r8mkintrp2d.f'
    ,'fortran/Pspline/r8mkintrp3d.f'
    ,'fortran/Pspline/r8mkspl2p.f'
    ,'fortran/Pspline/r8mkspl2pb.f'
    ,'fortran/Pspline/r8mkspl2z.f'
    ,'fortran/Pspline/r8mkspl2zb.f'
    ,'fortran/Pspline/r8mkspl3pb.f'
    ,'fortran/Pspline/r8mkspl3zb.f'
    ,'fortran/Pspline/r8mkspline.f'
    ,'fortran/Pspline/r8mktricub.f'
    ,'fortran/Pspline/r8mktricubw.f'
    ,'fortran/Pspline/r8nspline.f'
    ,'fortran/Pspline/r8pc1ev.f'
    ,'fortran/Pspline/r8pc2ev.f'
    ,'fortran/Pspline/r8pc3ev.f'
    ,'fortran/Pspline/r8psp_tolsum.f'
    ,'fortran/Pspline/r8pspline.f'
    ,'fortran/Pspline/r8seval.f'
    ,'fortran/Pspline/r8seval2.f'
    ,'fortran/Pspline/r8seval3.f'
    ,'fortran/Pspline/r8speval.f'
    ,'fortran/Pspline/r8spgrid.f'
    ,'fortran/Pspline/r8splaan.f'
    ,'fortran/Pspline/r8splbrk.f'
    ,'fortran/Pspline/r8spleen.f'
    ,'fortran/Pspline/r8splinck.f'
    ,'fortran/Pspline/r8spline.f'
    ,'fortran/Pspline/r8spvec.f'
    ,'fortran/Pspline/r8tcspeval.f'
    ,'fortran/Pspline/r8tcspgrid.f'
    ,'fortran/Pspline/r8tcspline.f'
    ,'fortran/Pspline/r8tcspvec.f'
    ,'fortran/Pspline/r8tpsplinb.f'
    ,'fortran/Pspline/r8tpspline.f'
    ,'fortran/Pspline/r8util_bcherm1.f'
    ,'fortran/Pspline/r8util_bcherm2.f'
    ,'fortran/Pspline/r8util_bcherm3.f'
    ,'fortran/Pspline/r8v_spline.f'
    ,'fortran/Pspline/r8vecbicub.f'
    ,'fortran/Pspline/r8vecherm1.f'
    ,'fortran/Pspline/r8vecherm2.f'
    ,'fortran/Pspline/r8vecherm3.f'
    ,'fortran/Pspline/r8vecintrp2d.f'
    ,'fortran/Pspline/r8vecintrp3d.f'
    ,'fortran/Pspline/r8vecpc1.f'
    ,'fortran/Pspline/r8vecpc2.f'
    ,'fortran/Pspline/r8vecpc3.f'
    ,'fortran/Pspline/r8vecspline.f'
    ,'fortran/Pspline/r8vectricub.f'
    ,'fortran/Pspline/r8xlookup.f'
    ,'fortran/Pspline/r8zonfind.f'
    ,'fortran/Pspline/seval.f'
    ,'fortran/Pspline/seval2.f'
    ,'fortran/Pspline/seval3.f'
    ,'fortran/Pspline/speval.f'
    ,'fortran/Pspline/spgrid.f'
    ,'fortran/Pspline/splaan.f'
    ,'fortran/Pspline/splbrk.f'
    ,'fortran/Pspline/spleen.f'
    ,'fortran/Pspline/splinck.f'
    ,'fortran/Pspline/spline_ez1.f'
    ,'fortran/Pspline/spvec.f'
    ,'fortran/Pspline/tcspeval.f'
    ,'fortran/Pspline/tcspgrid.f'
    ,'fortran/Pspline/tcspline.f'
    ,'fortran/Pspline/tcspvec.f'
    ,'fortran/Pspline/tpsplinb.f'
    ,'fortran/Pspline/tpspline.f'
    ,'fortran/Pspline/util_bcherm1.f'
    ,'fortran/Pspline/util_bcherm2.f'
    ,'fortran/Pspline/util_bcherm3.f'
    ,'fortran/Pspline/v_spline.f'
    ,'fortran/Pspline/vecbicub.f'
    ,'fortran/Pspline/vecherm1.f'
    ,'fortran/Pspline/vecherm2.f'
    ,'fortran/Pspline/vecherm3.f'
    ,'fortran/Pspline/vecin2d_argchk.f'
    ,'fortran/Pspline/vecin3d_argchk.f'
    ,'fortran/Pspline/vecintrp2d.f'
    ,'fortran/Pspline/vecintrp3d.f'
    ,'fortran/Pspline/vecpc1.f'
    ,'fortran/Pspline/vecpc2.f'
    ,'fortran/Pspline/vecpc3.f'
    ,'fortran/Pspline/vecspline.f'
    ,'fortran/Pspline/vectricub.f'
    ,'fortran/Pspline/xlookup.f'
    ,'fortran/Pspline/zonfind.f'
    ]

sources_libstell = [
    'fortran/Modules/stel_kinds.f90'
    ,'fortran/Modules/stel_constants.f90'
    ,'fortran/Modules/mpi_params.f90'
    ,'fortran/Modules/safe_open_mod.f90'
    ,'fortran/Modules/system_mod.f90'
    ,'fortran/Modules/read_wout_mini.f90'
    
    ,'fortran/Miscel/parse_extension.f90'
    ,'fortran/Miscel/pxffork.f'
    ,'fortran/Miscel/pxfwait.f'

    ,'fortran/Optimization/fdjac_mod.f90'
    ,'fortran/Optimization/lmpar_mod.f90'
    
    #'fortran/Optimization/DE2_Evolve.f'
    #,'fortran/Optimization/DE_Evaluate.f'
    #,'fortran/Optimization/DE_Evolve.f'
    #,'fortran/Optimization/DE_driver.f'
    #,'fortran/Optimization/DE_preset.f'
    #,'fortran/Optimization/GA_driver.f'
    #,'fortran/Optimization/GA_preset.f'
    #,'fortran/Optimization/GENERIC_Evolve.f90'
    #,'fortran/Optimization/MAP.f90'
    #,'fortran/Optimization/MAP_HYPERS.f90'
    #,'fortran/Optimization/MAP_LINEAR.f90'
    #,'fortran/Optimization/MAP_PLANE.f90'
    #,'fortran/Optimization/PSO_Evolve.f'
    #,'fortran/Optimization/ROCKET_Evolve.f90'
    #'fortran/Optimization/crosovr.f'
    #,'fortran/Optimization/de_mpi.f'
    #,'fortran/Optimization/de_parallel.f'
    ,'fortran/Optimization/dpmpar.f'
    ,'fortran/Optimization/enorm.f'
    #,'fortran/Optimization/eval_x_queued.f90'
    
    ,'fortran/Optimization/fdjac_parallel.f'
    #,'fortran/Optimization/ga_code.f'
    #,'fortran/Optimization/ga_decode.f'
    #,'fortran/Optimization/ga_evalout.f'
    #,'fortran/Optimization/ga_evaluate.f'
    #,'fortran/Optimization/ga_fitness_mpi.f'
    #,'fortran/Optimization/ga_fitness_parallel.f'
    #,'fortran/Optimization/ga_initial.f'
    #,'fortran/Optimization/ga_micro.f'
    #,'fortran/Optimization/ga_mutate.f'
    #,'fortran/Optimization/ga_newgen.f'
    #,'fortran/Optimization/ga_niche.f'
    #,'fortran/Optimization/ga_possibl.f'
    #,'fortran/Optimization/ga_restart.f'
    #,'fortran/Optimization/ga_select.f'
    #,'fortran/Optimization/ga_selectn.f'
    #,'fortran/Optimization/ga_shuffle.f'
    #,'fortran/Optimization/ga_sp.f'
    ,'fortran/Optimization/lmder_serial.f'
    ,'fortran/Optimization/lmdif.f'
    ,'fortran/Optimization/lmdif1.f'
    ,'fortran/Optimization/lmdif1_mp.f'
    ,'fortran/Optimization/lmpar.f'
    ,'fortran/Optimization/lmpar_parallel.f'
    ,'fortran/Optimization/lmpar_serial.f'
    ,'fortran/Optimization/multiprocess.f'
    ,'fortran/Optimization/myfork.f'
    ,'fortran/Optimization/qrfac.f'
    ,'fortran/Optimization/qrsolv.f'
    ,'fortran/Optimization/ran3.f'
    #,'fortran/Optimization/stepopt_mp.f'
    ,'fortran/Optimization/unique_boundary.f'
    ,'fortran/Optimization/unique_boundary_PG.f'
    #,'fortran/Optimization/write_gade_nml.f'
    #,'fortran/Optimization/xvec_eval.f90'
    ]
    
sources_stelltools = [
    'fortran/stelltools.f90'
    ]

sources = [
    'fortran/stelltools_wrapper.pyf'
    ,'fortran/stelltools_wrapper.f90'
    ]

# The order is important here.
sources += sources_ezcdf
sources += sources_pspline
sources += sources_libstell
sources += sources_stelltools

ext = Extension(
    name = 'stelltools_fortran'
    ,sources = sources
    ,libraries = ['netcdf', 'netcdff']
    ,library_dirs = library_dirs
    ,include_dirs = include_dirs
    ,extra_f77_compile_args = ['-fallow-argument-mismatch']
    ,extra_f90_compile_args = ['-fallow-argument-mismatch']
    )

# Define options for setup.
params = {'name':'stelltools'
          ,'version':__version__
          ,'description':'A VMEC tool to perform conversions between flux surface coordinates and real coordinates.'
          ,'long_description':long_description
          ,'author':'Novimir Antoniuk Pablant'
          ,'author_email':'novimir.pablant@amicitas.com'
          ,'url':'http://amicitas.bitbucket.org/mirtools/'
          ,'license':'MIT'
          ,'classifiers':classifiers
          #,'install_requires':['numpy']
          ,'ext_package':'stelltools'
          ,'ext_modules':[ext]
          ,'packages':['stelltools']
          ,'cmdclass':{'build':build_preprocess}
          }
        
if __name__ == "__main__":
    
    # Call the actual building/packaging function (see distutils docs)
    setup(**params)
